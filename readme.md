# Global Automation API Tests

Tests written using [Postman](https://www.postman.com/) - The Collaboration Platform for API Development

# Getting started - Postman
1. Install Postman
2. Get the latest collection from https://api.getpostman.com/collections/14016400-36a1d915-a714-4644-a0a2-d6c6d7b3831d?apikey=PMAK-5fec43f26c262d00434f6dec-4eae12f5b24ba45361ac208cadd7383617
3. Import collection into postman
4. You may now add/edit requests or run the collection

# Getting started - newman
[Newman](https://www.npmjs.com/package/newman) is a command-line collection runner for Postman.

1. Install newman: ***npm install -g newman***
2. Run tests with ***newman run <collection>***

# Tagging tests
You can include tags in the requests names to filter them at execution. The tags should be added in the begining of the name between square parentheses. Examples of valid names with tags:
- [BUG]get expense invoiced
- [WIP]test get login
- [BUG,no-prod,GLO-1234]test GET clients

Currently, the script includes tags used for filtering out the requests from execution; those are: BUG, WIP, DONT_RUN.