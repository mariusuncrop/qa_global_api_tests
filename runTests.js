const fetch = require('node-fetch');
const newman = require('newman');
var fs = require('fs');
const { type } = require('os');
const { request } = require('http');

let collectionUrl = "https://api.getpostman.com/collections/3b863b49-fec5-481a-ad76-bf8dd3c1609f?apikey=PMAK-5fec43f26c262d00434f6dec-4eae12f5b24ba45361ac208cadd7383617";
let collectionPath = './tests/Global_API_Tests.postman_collection.json'
var collectionJson;
let temporaryCollectionPath = 'temp/temporaryCollection.json';
var environmentConfigFile;

const IGNORED_TAGS = ['BUG', 'DONT_RUN', 'WIP'];

const envParam = process.argv[2];
switch (envParam) {
    case 'acting-qa':
        environmentConfigFile = checkPostmanCloud() ? "https://api.getpostman.com/environments/14016400-b87c8b02-77a2-4993-b8e3-a81f7d05cd4c?apikey=PMAK-5fec43f26c262d00434f6dec-4eae12f5b24ba45361ac208cadd7383617" : './tests/acting-qa.postman_environment.json';
        break;
    default:
        console.log("Invalid value for environment name: " + envParam);
        process.exit(9);
}

function getRequestTags(requestName) {
    var tags = []
    try {
        var tagsString = requestName.match(/(?<=\[)(.*)(?=\])/)[0];
        tags = tagsString.split(",");
    }
    catch {
        tags = []
    }
    return tags;
}

function checkPostmanCloud() {
    const cloudParam = process.argv[3];
    if (cloudParam == 'cloud') {
        return true;
    }
    return false;
}

function checkIfIgnore(requestName) {
    var should_ignore = false;
    tags = getRequestTags(requestName);
    tags.forEach(tag => {
        if (IGNORED_TAGS.includes(tag.trim())) { should_ignore = true; };
    })
    return should_ignore;
}

async function getCollectionJSONFromCloud() {
    console.log("Getting the collection json file from the postman cloud\n.....")
    const response = await fetch(collectionUrl, {method: 'Get'});
    const json = await response.json();
    console.log("Successfully got the collection json file from the postman cloud!")
    return json.collection;
}

async function readCollectionJSONFromFile() {
    console.log("Reading the collection json file from local directory\n....");
    let json = require(collectionPath);
    console.log("Successfully read the collection json file from local directory!");
    return json;
}

async function filterRequestsFromFolder(folderJSON) {
    var requestsToRemove = [];
    folderJSON.item.forEach(item => {
        itemIndex = folderJSON.item.indexOf(item);
        if (checkIfIgnore(item.name)) {
            requestsToRemove.push(item);
        }
        if (item.item) {
            filteredItem = filterRequestsFromFolder(item);
            // folderJSON.item[folderJSON.item.indexOf(item)] = filteredItem;
        }
    });
    requestsToRemove.forEach(request => {
        folderJSON.item.splice(folderJSON.item.indexOf(request), 1);
    });
    return folderJSON;
}

async function filterAllRequests(collectionJSON) {
    console.log("Starting filtering the collection requests based on tags\n....")
    collectionJSON = await filterRequestsFromFolder(collectionJSON);
    fs.writeFile(temporaryCollectionPath, JSON.stringify(collectionJson), function (err) {
        if (err) {
            console.log(err);
        }
    });
    return collectionJSON;
    console.log("Successfully filtered the requests based on tags!");
    console.log("Temporary json collection file was successfully created!")
}

async function runTests() {
    collectionJson = checkPostmanCloud() ? await getCollectionJSONFromCloud() : await readCollectionJSONFromFile();
    await filterAllRequests(collectionJson);

    console.log("\nStarting running the tests .... \n\n\n\n\n\n\n\n")
    newman.run({
        collection: temporaryCollectionPath,
        environment: environmentConfigFile,
        reporters: ['cli', 'junit', 'html', 'htmlextra', 'testrail'],
        reporter: {
            junit: {
                export: "`./test-reports/junitResults.xml`"
            },
            html: {
                export: "test-reports/htmlReport.html"
            },
            htmlextra: {
                export: "test-reports/htmlextraReport.html",
                browserTitle: "Global API Tests",
                title: "Smoke suite run results"
            }
        }
    })
}

runTests()