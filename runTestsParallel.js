#!/usr/bin/env node
/**
 * @fileOverview This sample code illustrates how one can read all collection files within a directory and run them
 * in parallel.
 */
const fetch = require('node-fetch');
const newman = require ('newman');
const async = require('async'); 
var fs = require('fs');

let collectionUrl = "https://api.getpostman.com/collections/14016400-36a1d915-a714-4644-a0a2-d6c6d7b3831d?apikey=PMAK-5fec43f26c262d00434f6dec-4eae12f5b24ba45361ac208cadd7383617";
// collectionUrl = "https://api.getpostman.com/collections/14016400-ccadf70a-1a64-4a9b-b53d-27854939f71e?apikey=PMAK-5fec43f26c262d00434f6dec-4eae12f5b24ba45361ac208cadd7383617";
var collectionJson;
let actingqaEnv = "https://api.getpostman.com/environments/14016400-b87c8b02-77a2-4993-b8e3-a81f7d05cd4c?apikey=PMAK-5fec43f26c262d00434f6dec-4eae12f5b24ba45361ac208cadd7383617"
let folder1 = '1a2d2a33-7b0b-4987-802d-6b3b692db511'


var parallelRuns = []
var totalResults = []
fetch(collectionUrl, { method: "Get" })
    .then(res => res.json())
    .then((json) => {
        json.collection.item.forEach(folder => {
            console.log(folder._postman_id)
            parallelRuns.push(function (done) {
                newman.run({
                    collection:collectionUrl,
                    folder: folder._postman_id,
                    environment: actingqaEnv,
                    reporters: ['cli', 'junit'],
                    reporter : { junit : { export : `./test-reports/junitResults${folder.name}.xml` } }
                }, done)
            })

        });

        console.log(`\n\n\n\n\n\n\n\n\n\n\n\n\n############################################### ${parallelRuns}  \n\n\n\n\n\n\n\n\n\n`)

        
        async.parallel(parallelRuns,

            /**
             * The
             *
             * @param {?Error} err - An Error instance / null that determines whether or not the parallel collection run
             * succeeded.
             * @param {Array} results - An array of collection run summary objects.
             */
            function (err, results) {
                err && console.error(err);
            
                results.forEach(function (result) {
                    totalResults.push(result)
                    var failures = result.run.failures;
            
                    console.info(failures.length ? JSON.stringify(failures.failures, null, 2) :
                        `${result.collection.name} ran successfully.`);
                });

            });
            console.log(`Total number of results : ${totalResults.length} \n\n\n\n\n\n\n\n\n`)

    });

